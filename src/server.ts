import * as async from 'async';
import * as bodyParser from 'body-parser';
import * as cheerio from 'cheerio';
import * as compression from 'compression';
import * as cors from 'cors';
import { NextFunction, Request, Response } from 'express';
import * as express from 'express';
import * as helmet from 'helmet';
import * as https from 'https';
import * as request from 'request';
import { isNullOrUndefined } from 'util';
import * as XLSX from 'xlsx';
import mongoDbDataAccessLayer from './mongodb';
class Server {
  // set app to be of type express.Application
  public app: express.Application;

  constructor() {
    this.app = express();
    this.config();
    this.routes();
  }

  // application config
  public config(): void {
    // express middleware
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(bodyParser.json());
    this.app.use('/files', express.static('files'));
    this.app.use(compression());
    this.app.use(helmet());
    // this.app.use(cors());
    this.app.use(cors({ exposedHeaders: ['Authorization'] }));

    this.app.use((req: Request, res: Response, next: NextFunction) => {
      res.setHeader('Last-Modified', (new Date()).toUTCString());
      req.headers['if-none-match'] = 'no-match-for-this';
      res.header('Access-Control-Allow-Origin', '*');
      res.header(
        'Access-Control-Allow-Methods',
        'GET, POST, PUT, DELETE, OPTIONS',
      );
      res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Credentials',
      );
      res.header('Access-Control-Allow-Credentials', 'true');
      next();
    });
  }

  // application routes
  public routes(): void {
    const version = '/api/v1/';
    const router: express.Router = express.Router();
    this.app.use('/', router);
    this.scrapeEmailInit_two();
    // this.writeXlsx();
  }
  // Section One Start
  private scrapeEmailInit() {
    // tslint:disable-next-line: max-line-length
    const url: string = 'https://journals.plos.org/plosone/dynamicSearch?filterJournals=PLoSONE&resultsPerPage=1&q=disorder&sortOrder=DATE_NEWEST_FIRST&page=1';
    https.get(url, (response) => {
      let data: string = '';
      response.on('data', (chunk) => {
        data += chunk;
      });
      response.on('end', () => {
        console.info('Total no.of journals: ' + JSON.parse(data).searchResults.numFound);
        this.scrapeEmails(900, JSON.parse(data).searchResults.numFound);
        // this.scrapeEmails(900, 900);
      });
    });
  }
  private scrapeEmails(perPage: number, totalRecords: number) {
    let pageNumber: number = 1;
    const maxPages: number = Math.ceil(totalRecords / perPage);
    let journals: object[] = [];
    async.whilst(() => maxPages >= pageNumber, (next) => {
      console.info('Loading page ' + pageNumber + ' out of ' + maxPages);
      // tslint:disable-next-line: max-line-length
      const url: string = `https://journals.plos.org/plosone/dynamicSearch?filterJournals=PLoSONE&resultsPerPage=${perPage}&q=disorder&sortOrder=DATE_NEWEST_FIRST&page=${pageNumber}`;
      https.get(url, (response) => {
        let data: string = '';
        response.on('data', (chunk) => {
          data += chunk;
        });
        response.on('end', () => {
          journals = journals.concat(JSON.parse(data).searchResults.docs);
          // if (maxPages === pageNumber) {
          journals.map((mapData) => {
            const link = 'https://journals.plos.org' + mapData['link'].replace('article', 'article/authors');
            mapData['link'] = link;
            return mapData;
          });
          console.info('Scrapping Completed');
          journals.forEach((item, i) => {
            request(item['link'], (error, res, html) => {
              if (!error && res.statusCode === 200) {
                const $ = cheerio.load(html);
                $('section.authors').find('dl').find('dd').toArray().forEach((ele, ind) => {
                  if (!isNullOrUndefined($(ele).find($('span.email')).html())) {
                    // tslint:disable-next-line: max-line-length
                    item['author_name'] = $($('section.authors').find('dl').find('dt').toArray()[ind]).html().trim();
                  }
                });
                item['email'] = $('span.email').parent().children('a').html();
              }
              if (i === (journals.length - 1)) {
                const dt = journals;
                journals = [];
                this.add(dt, pageNumber);
              }
            });
          });
          // }
          pageNumber++;
          next();
        });
      });
    });
  }
  private add(journals: object[], set: number) {
    try {
      // tslint:disable-next-line:max-line-length
      mongoDbDataAccessLayer.MongoClient.connect(mongoDbDataAccessLayer.url, { useNewUrlParser: true, useUnifiedTopology: true }, (err, db) => {
        if (err) { throw err; }
        const dataBase = db.db(mongoDbDataAccessLayer.dataBaseName);
        // tslint:disable-next-line:max-line-length
        const collection = dataBase.collection('journals_plos_1');
        journals = journals.map((itemMap) => {
          return {
            title: itemMap['title'],
            author_name: itemMap['author_name'],
            email: itemMap['email'],
            publication_date: new Date(itemMap['publication_date']),
            link: itemMap['link']
          };
        });
        collection.insertMany(journals).then(() => {
          console.log(set + ' insert completed');
        });
      });
    } catch (error) { console.log(error); }
  }
  private writeXlsx() {
    // tslint:disable-next-line:max-line-length
    mongoDbDataAccessLayer.MongoClient.connect(mongoDbDataAccessLayer.url, { useNewUrlParser: true, useUnifiedTopology: true }, (err, db) => {
      if (err) { throw err; }
      const dataBase = db.db(mongoDbDataAccessLayer.dataBaseName);
      // tslint:disable-next-line:max-line-length
      const collection = dataBase.collection('journals_plos');
      collection.find({}).project({ _id: 0 }).toArray((collectionErr, data) => {
        if (collectionErr) { console.log(err); }
        const ws = XLSX.utils.json_to_sheet(data);
        const wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'Journals PLOS');
        XLSX.writeFile(wb, 'journals_plos.xlsx');
        console.log('Completed');
      });
    });
  }
  // Section One End

  // Section Two Start
  private scrapeEmailInit_two() {
    // tslint:disable-next-line: max-line-length
    const url: string = 'https://koreamed.org/search/result?q=korea&resultsPerPage=1&page=1&display=Summary&sort=Date';
    https.get(url, (response) => {
      let data: string = '';
      response.on('data', (chunk) => {
        data += chunk;
      });
      response.on('end', () => {
        console.info('Total no.of journals: ' + JSON.parse(data).results.result.count);
        this.scrapeEmails_two(1000, JSON.parse(data).results.result.count);
        // this.scrapeEmails(900, 900);
      });
    });
  }
  private scrapeEmails_two(perPage: number, totalRecords: number) {
    let pageNumber: number = 1;
    const maxPages: number = Math.ceil(totalRecords / perPage);
    let journals: object[] = [];
    async.whilst(() => maxPages >= pageNumber, (next) => {
      journals = [];
      console.info('Loading page ' + pageNumber + ' out of ' + maxPages);
      // tslint:disable-next-line: max-line-length
      const url: string = `https://koreamed.org/search/result?q=korea&resultsPerPage=${perPage}&page=${pageNumber}&display=Summary&sort=Date`;
      https.get(url, (response) => {
        let data: string = '';
        response.on('data', (chunk) => {
          data += chunk;
        });
        response.on('end', () => {
          journals = journals.concat(JSON.parse(data).results.data);
          // if (maxPages === pageNumber) {
          const check = journals.map((mapData) => {
            if (!isNullOrUndefined(mapData['affiliate_facet']) && mapData['affiliate_facet'].length > 0) {
              mapData['affiliate_facet'].forEach((element) => {
                if (this.checkIfEmailInString(element)) { mapData['email'] = this.extractEmails(element)[0]; }
              });
            }
            // tslint:disable-next-line: max-line-length
            if (!isNullOrUndefined(mapData['author_facet']) && mapData['author_facet'].length > 0) { mapData['authors'] = mapData['author_facet'].join(':-'); }
            return { title: mapData['title'], authors: mapData['authors'], email: mapData['email'] };
          });
          console.info('Scrapping Completed');
          // if (pageNumber === maxPages) {
          // tslint:disable-next-line: only-arrow-functions
          const fs = require('fs');
          const csvWriter = require('csv-write-stream');
          let writer = csvWriter({ sendHeaders: false });
          const csvFilename = 'C:/Users/21218/Desktop/synapse.csv';

          // If CSV file does not exist, create it and add the headers
          if (!fs.existsSync(csvFilename)) {
            writer = csvWriter({ sendHeaders: false });
            writer.pipe(fs.createWriteStream(csvFilename));
            writer.write({
              header1: 'Title',
              header2: 'Author',
              header3: 'Email'
            });
            writer.end();
          }
          check.forEach((result) => {
            // Append some data to CSV the file    
            writer = csvWriter({ sendHeaders: false });
            writer.pipe(fs.createWriteStream(csvFilename, { flags: 'a' }));
            writer.write({
              header1: result['title'],
              header2: result['authors'],
              header3: result['email']
            });
            writer.end();

          });

          // }
          pageNumber++;
          next();
        });
      });
    });
  }
  public extractEmails(text: string) {
    return text.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi);
  }
  public checkIfEmailInString(text: string) {
    // tslint:disable-next-line: max-line-length
    const re = /(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/;
    return re.test(text);
  }
}

// export
export default new Server().app;