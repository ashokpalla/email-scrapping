"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const async = require("async");
const bodyParser = require("body-parser");
const cheerio = require("cheerio");
const compression = require("compression");
const cors = require("cors");
const express = require("express");
const helmet = require("helmet");
const https = require("https");
const request = require("request");
const util_1 = require("util");
const XLSX = require("xlsx");
const mongodb_1 = require("./mongodb");
class Server {
    constructor() {
        this.app = express();
        this.config();
        this.routes();
    }
    // application config
    config() {
        // express middleware
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(bodyParser.json());
        this.app.use('/files', express.static('files'));
        this.app.use(compression());
        this.app.use(helmet());
        // this.app.use(cors());
        this.app.use(cors({ exposedHeaders: ['Authorization'] }));
        this.app.use((req, res, next) => {
            res.setHeader('Last-Modified', (new Date()).toUTCString());
            req.headers['if-none-match'] = 'no-match-for-this';
            res.header('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
            res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Credentials');
            res.header('Access-Control-Allow-Credentials', 'true');
            next();
        });
    }
    // application routes
    routes() {
        const version = '/api/v1/';
        const router = express.Router();
        this.app.use('/', router);
        this.scrapeEmailInit_two();
        // this.writeXlsx();
    }
    // Section One Start
    scrapeEmailInit() {
        // tslint:disable-next-line: max-line-length
        const url = 'https://journals.plos.org/plosone/dynamicSearch?filterJournals=PLoSONE&resultsPerPage=1&q=disorder&sortOrder=DATE_NEWEST_FIRST&page=1';
        https.get(url, (response) => {
            let data = '';
            response.on('data', (chunk) => {
                data += chunk;
            });
            response.on('end', () => {
                console.info('Total no.of journals: ' + JSON.parse(data).searchResults.numFound);
                this.scrapeEmails(900, JSON.parse(data).searchResults.numFound);
                // this.scrapeEmails(900, 900);
            });
        });
    }
    scrapeEmails(perPage, totalRecords) {
        let pageNumber = 1;
        const maxPages = Math.ceil(totalRecords / perPage);
        let journals = [];
        async.whilst(() => maxPages >= pageNumber, (next) => {
            console.info('Loading page ' + pageNumber + ' out of ' + maxPages);
            // tslint:disable-next-line: max-line-length
            const url = `https://journals.plos.org/plosone/dynamicSearch?filterJournals=PLoSONE&resultsPerPage=${perPage}&q=disorder&sortOrder=DATE_NEWEST_FIRST&page=${pageNumber}`;
            https.get(url, (response) => {
                let data = '';
                response.on('data', (chunk) => {
                    data += chunk;
                });
                response.on('end', () => {
                    journals = journals.concat(JSON.parse(data).searchResults.docs);
                    // if (maxPages === pageNumber) {
                    journals.map((mapData) => {
                        const link = 'https://journals.plos.org' + mapData['link'].replace('article', 'article/authors');
                        mapData['link'] = link;
                        return mapData;
                    });
                    console.info('Scrapping Completed');
                    journals.forEach((item, i) => {
                        request(item['link'], (error, res, html) => {
                            if (!error && res.statusCode === 200) {
                                const $ = cheerio.load(html);
                                $('section.authors').find('dl').find('dd').toArray().forEach((ele, ind) => {
                                    if (!util_1.isNullOrUndefined($(ele).find($('span.email')).html())) {
                                        // tslint:disable-next-line: max-line-length
                                        item['author_name'] = $($('section.authors').find('dl').find('dt').toArray()[ind]).html().trim();
                                    }
                                });
                                item['email'] = $('span.email').parent().children('a').html();
                            }
                            if (i === (journals.length - 1)) {
                                const dt = journals;
                                journals = [];
                                this.add(dt, pageNumber);
                            }
                        });
                    });
                    // }
                    pageNumber++;
                    next();
                });
            });
        });
    }
    add(journals, set) {
        try {
            // tslint:disable-next-line:max-line-length
            mongodb_1.default.MongoClient.connect(mongodb_1.default.url, { useNewUrlParser: true, useUnifiedTopology: true }, (err, db) => {
                if (err) {
                    throw err;
                }
                const dataBase = db.db(mongodb_1.default.dataBaseName);
                // tslint:disable-next-line:max-line-length
                const collection = dataBase.collection('journals_plos_1');
                journals = journals.map((itemMap) => {
                    return {
                        title: itemMap['title'],
                        author_name: itemMap['author_name'],
                        email: itemMap['email'],
                        publication_date: new Date(itemMap['publication_date']),
                        link: itemMap['link']
                    };
                });
                collection.insertMany(journals).then(() => {
                    console.log(set + ' insert completed');
                });
            });
        }
        catch (error) {
            console.log(error);
        }
    }
    writeXlsx() {
        // tslint:disable-next-line:max-line-length
        mongodb_1.default.MongoClient.connect(mongodb_1.default.url, { useNewUrlParser: true, useUnifiedTopology: true }, (err, db) => {
            if (err) {
                throw err;
            }
            const dataBase = db.db(mongodb_1.default.dataBaseName);
            // tslint:disable-next-line:max-line-length
            const collection = dataBase.collection('journals_plos');
            collection.find({}).project({ _id: 0 }).toArray((collectionErr, data) => {
                if (collectionErr) {
                    console.log(err);
                }
                const ws = XLSX.utils.json_to_sheet(data);
                const wb = XLSX.utils.book_new();
                XLSX.utils.book_append_sheet(wb, ws, 'Journals PLOS');
                XLSX.writeFile(wb, 'journals_plos.xlsx');
                console.log('Completed');
            });
        });
    }
    // Section One End
    // Section Two Start
    scrapeEmailInit_two() {
        // tslint:disable-next-line: max-line-length
        const url = 'https://koreamed.org/search/result?q=korea&resultsPerPage=1&page=1&display=Summary&sort=Date';
        https.get(url, (response) => {
            let data = '';
            response.on('data', (chunk) => {
                data += chunk;
            });
            response.on('end', () => {
                console.info('Total no.of journals: ' + JSON.parse(data).results.result.count);
                this.scrapeEmails_two(1000, JSON.parse(data).results.result.count);
                // this.scrapeEmails(900, 900);
            });
        });
    }
    scrapeEmails_two(perPage, totalRecords) {
        let pageNumber = 1;
        const maxPages = Math.ceil(totalRecords / perPage);
        let journals = [];
        async.whilst(() => maxPages >= pageNumber, (next) => {
            journals = [];
            console.info('Loading page ' + pageNumber + ' out of ' + maxPages);
            // tslint:disable-next-line: max-line-length
            const url = `https://koreamed.org/search/result?q=korea&resultsPerPage=${perPage}&page=${pageNumber}&display=Summary&sort=Date`;
            https.get(url, (response) => {
                let data = '';
                response.on('data', (chunk) => {
                    data += chunk;
                });
                response.on('end', () => {
                    journals = journals.concat(JSON.parse(data).results.data);
                    // if (maxPages === pageNumber) {
                    const check = journals.map((mapData) => {
                        if (!util_1.isNullOrUndefined(mapData['affiliate_facet']) && mapData['affiliate_facet'].length > 0) {
                            mapData['affiliate_facet'].forEach((element) => {
                                if (this.checkIfEmailInString(element)) {
                                    mapData['email'] = this.extractEmails(element)[0];
                                }
                            });
                        }
                        // tslint:disable-next-line: max-line-length
                        if (!util_1.isNullOrUndefined(mapData['author_facet']) && mapData['author_facet'].length > 0) {
                            mapData['authors'] = mapData['author_facet'].join(':-');
                        }
                        return { title: mapData['title'], authors: mapData['authors'], email: mapData['email'] };
                    });
                    console.info('Scrapping Completed');
                    // if (pageNumber === maxPages) {
                    // tslint:disable-next-line: only-arrow-functions
                    const fs = require('fs');
                    const csvWriter = require('csv-write-stream');
                    let writer = csvWriter({ sendHeaders: false });
                    const csvFilename = 'C:/Users/21218/Desktop/synapse.csv';
                    // If CSV file does not exist, create it and add the headers
                    if (!fs.existsSync(csvFilename)) {
                        writer = csvWriter({ sendHeaders: false });
                        writer.pipe(fs.createWriteStream(csvFilename));
                        writer.write({
                            header1: 'Title',
                            header2: 'Author',
                            header3: 'Email'
                        });
                        writer.end();
                    }
                    check.forEach((result) => {
                        // Append some data to CSV the file    
                        writer = csvWriter({ sendHeaders: false });
                        writer.pipe(fs.createWriteStream(csvFilename, { flags: 'a' }));
                        writer.write({
                            header1: result['title'],
                            header2: result['authors'],
                            header3: result['email']
                        });
                        writer.end();
                    });
                    // }
                    pageNumber++;
                    next();
                });
            });
        });
    }
    extractEmails(text) {
        return text.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi);
    }
    checkIfEmailInString(text) {
        // tslint:disable-next-line: max-line-length
        const re = /(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/;
        return re.test(text);
    }
}
// export
exports.default = new Server().app;
